[plantuml, livre-chaine-modulaire, svg]
....
skinparam monochrome true
skinparam defaultFontSize 16
skinparam defaultFontName Fira Mono
skinparam shadowing false
top to bottom direction

cloud "Phases d'écriture, de validation, de composition\nVersionnement des fichiers avec Git\n" {

node Contenus as content {
  node "Textes\n(langage de balisage léger)" as texte
  node "Médias\n(images, etc.)" as media
  node "Données" as data
  node "Modèles\n(feuilles de style)" as styles
}

}

texte -right- media
texte -down- data
media -down- styles
data -right- styles

node "**Processeur**\nGénérateur\nde site statique" as gss
node "**Générateur PDF**\n(Prince XML / paged.js)" as procpdf

cloud "**HTML**\n**CSS**\n**JS**\n**JSON**" as html {

}

content -down--> gss
gss -down-> html
html -left-> procpdf
procpdf -left-> (PDF imprimable)
html -right-> (Site web)
html -down-> (EPUB)
....
