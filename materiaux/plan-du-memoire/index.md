# Plan du mémoire

Plan détaillé du mémoire sur les chaînes de publication.

## Objectifs
Le mémoire se veut "modulaire", il doit intégrer les différentes étapes préparatoires, et non être rédigé de façon non linéaire : une approche plus traditionnelle est de réaliser d'abord un travail de recherche (lectures, analyses de cas), puis de rédiger le mémoire et d'indiquer le travail de recherche en annexes.

Ici je souhaite faire un assemblage des commentaires de texte, des analyses de cas et, éventuellement, des entretiens, pour constituer quatre parties distinctes :

1. une première partie *théorique* basée sur des commentaires de texte ;
2. une seconde partie *pratique* consacrée à l'analyse de différents cas ;
3. une troisième partie sous forme d'une présentation d'un nouveau modèle de chaîne de publication ;
4. une quatrième partie réflexive avec des entretiens de professionnels. **Cette partie sera travaillée après la livraison du mémoire mais si possible avant la soutenance.**

## Plan

### Introduction (7k)
Objectif : en plus d'introduire le sujet, de poser la problématique et de présenter le plan, l'introduction doit exposer la relative originalité du plan, et les modes de lecture possibles.  
Résumé :

* bouleversements induits par le numérique sur le livre ;
* bouleversements provoqués par le livre numérique ;
* exemples de _chaînes_ originales ;
* nécessité de repenser la façon de concevoir, fabriquer et produire des livres ou plus largement des publications ;
* problématique et plan ;
* originalité de la forme du mémoire (travaux, formats, lectures).

### 1. Mutations du livre (67k)
Le numérique a influencé la distribution, la diffusion, la production et la conception du livre.
La chaîne de publication doit être le nouveau centre de préoccupation de la mutation du livre, car c'est à partir de là que peuvent se déployer différentes opportunités ou concrétisations.
Le solutionnisme a été, depuis l'arrivée des outils de publication assistée par ordinateur, l'approche privilégiée pour la fabrication du livre, depuis les méthodes jusqu'aux outils utilisés.
Il est désormais nécessaire de dépasser cela, d'autres technologies et processus permettent d'envisager non plus une _chaîne_ mais un système de publication, cohérent et modulaire.

#### 1.1. Hybridation (18k)
Ou pourquoi il est nécessaire d'observer les évolutions du livre au prisme du numérique.
Sur la base du commentaire de texte de *Post-Digital Print*.

#### 1.2. Solutionnisme (26k)
Ou pourquoi il faut dépasser le logiciel et les méthodes linéaires et verticales.
Sur la base du commentaire de texte de *Pour tout résoudre cliquez ici*.

#### 1.3. Progrès technique (23k)
Ou comment la pensée de Gilbert Simondon et sa vision du "progrès technique" donne des pistes pour penser une nouvelle chaîne de publication.  
Sur la base du commentaire de texte de *Du mode d'existence des objets techniques*.


### 2. Expérimentations (24k)
Depuis une vingtaine d'années, et malgré l'essor des logiciels de traitement de texte et de publication assistée par ordinateur, plusieurs expérimentations tendent à se baser sur des standards plutôt que sur des logiciels.
Des éditeurs ou des designers ont mis en place des nouvelles chaînes de publication, depuis l'usage du code informatique standardisé jusqu'à l'influence des méthodes et technologies du développement web.

#### 2.1. O'Reilly (8k)
O'Reilly et l'évolution de standards de publication : O'Reilly Media est l'une des premières structures d'édition à réaliser d'importantes recherches autour des formats puis des workflows utilisés pour la conception des livres, d'un schéma XML à la mise en place d'une plateforme, en passant par l'utilisation de Git ou d'HTML5.

#### 2.2. PrePostPrint (8k)
Vers des technologies plus accessibles, libres et conviviales : PrePostPrint.
En parallèle des pratiques de certaines maisons d'édition, des designers travaillent sur des techniques de publication expérimentales, notamment autour d'HTML et CSS.
PrePostPrint rassemble les initiatives qui considèrent que la programmation devient un outil de design.

#### 2.3. Getty Publications (8k)
Le web comme principe d'une chaîne de publication : Getty Publications réalise un prototype de chaîne de publication utilisant un générateur de site statique.
Originale et performante, cette nouvelle pratique implique des changements dans les pratiques des éditeurs.


### 3. Un système de publication modulaire inspiré du Web (80k)

### 3.0. Introduction (10k)
À partir de l'analyse théorique du premier chapitre, et des analyses de cas du second chapitre, nous reprenons les étapes d'une chaîne de publication et ses contraintes, et nous dessinons les trois principes d'une nouvelle chaîne de publication : interopérable, modulaire et multiforme.
Préambule nécessaire sur les limites des systèmes classiques.

#### 3.1. Les étapes d’une chaîne de publication (30k)
Afin de bien comprendre le fonctionnement d’une chaîne de publication et les besoins qui y sont liés, nous présentons les différentes étapes de conception d’un livre, et les propositions pour mettre en place un *workflow* modulaire.

##### 3.1.1. Écrire et structurer
Inscrire du texte, structurer l'information : comment cela est-il possible aujourd'hui ? Quelles contraintes ? Comment les contourner ?

##### 3.1.2. Partager, collaborer et valider
Éditer un texte, et travailler à plusieurs pour réaliser cette édition.

##### 3.1.3. Mettre en forme et générer les formats
Qualifier les informations via la structure du texte, et produire les différents formats du livre.

##### 3.1.4. Publier
Rendre disponible un texte, que ce soit sous la forme d'un livre imprimé, d'un livre numérique ou d'une base de données.

#### 3.2. Les principes d'un nouveau modèle de chaîne de publication (40k)
Trois principes, liés entre eux, sont au centre d'un nouveau modèle de système de publication : l'interopérabilité, la modularité et le fait d'être multiforme.

##### 3.2.1. Interopérabilité
Utiliser des formats et des standards pour faciliter le travail à plusieurs, sans logiciel fermé, et dans le temps long.
Grâce à cela l'usage de Git est possible.

##### 3.2.2. Modularité
L'interopérabilité ouvre la possibilité de ne pas dépendre d'un logiciel précis, et donc d'envisager une chaîne modulaire : chaque fonctionnalité est une brique à laquelle un ou des outils peuvent être attribués.

##### 3.2.3. Multiforme
Grâce à une approche modulaire la chaîne de publication peut, potentiellement, générer de nouvelles formes ou de nouveaux formats.


### 4. En option : des principes à l'épreuve du terrain (25k)
Les bases théoriques et les analyses de cas ayant été exposées, nous mettons à l'épreuve les principes précédemment énoncés par le biais d'entretiens avec des professionnels de l'édition, ayant ou non expérimentés une telle approche.

#### 4.1. Hybridation 1
C&F éditions : utilisation de XML *et* d'InDesign, limite de ce modèle et complexité d'évolution.

#### 4.2. Hybridation 2
McMillan : intégration d'HTML et de CSS dans la chaîne de publication, tout en conservant un traitement de texte.

#### 4.3. Recherche et développement
Getty Publication : étape suivante par rapport à McMillan, implication pour l'équipe, difficultés rencontrées et défis à relever. Il s'agit de la suite d'un premier entretien déjà publié.

### Conclusion (7k)
Reprise des trois commentaires et des trois analyses : interconnexion entre des aspects théoriques et des aspects pratiques.
