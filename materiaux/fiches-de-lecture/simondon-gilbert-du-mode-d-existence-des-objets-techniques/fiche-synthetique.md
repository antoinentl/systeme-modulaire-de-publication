# Fiche synthétique : Simondon, Gilbert, Du mode d'existence des objets techniques

## Notice bibliographique de l'ouvrage étudié
Simondon Gilbert  
*Du mode d'existence des objets techniques*  
Paris, Aubier, 2012


## Introduction
*Situer brièvement le texte dans les premières lignes (par rapport à la doctrine entière, par rapport à l'œuvre dont il est issu, par rapport à la philosophie en général), puis dans les lignes suivantes vous pourrez alors exposer le thème débouchant sur la thèse, puis sur la problématique du texte.*

## 1. Analyse des formes grammaticales ou générales

### 1.1. Présentation générale du texte (paragraphes, etc.)
La structure générale est classique, en trois niveaux : partie puis chapitre puis sous-partie. Chaque sous-partie est relativement courte, et toujours cohérente : développement d'un argument qui regroupe des idées et des concepts, idées et concepts qui seront ensuite réutilisés dans les parties suivantes.

Le texte se présente sous une forme assez *ramassée* : des paragraphes d'une demi-page ou d'une page maximum. Les paragraphes sont séparés d'un simple saut de ligne lorsque l'auteur développe un même concept, et un saut de paragraphe vient signifier le développement d'un nouveau concept ou d'une précision importante. Le texte est dense mais lisible, les respirations nécessaires sont présentes.

Nous pouvons noter l'utilisation d'exemples, assez longs, qui sont partie intégrante de l'argumentation de Gilbert Simondon. Des systèmes techniques sont décrits avec une grande précision, un lexique des mots techniques est par ailleurs placé à la fin du livre. Lors de la lecture nous pouvons nous rendre compte que ces exemples servent également à appuyer et à expliquer plus longuement certains concepts complexes, donnant à le temps et l'espace pour penser les arguments de Gilbert Simondon.


### 1.2. Termes ou expressions de liaison
Le texte de Gilbert Simondon comporte peu de termes ou d'expressions de liaison entre paragraphes, on trouve toutefois des connecteurs classiques dans les textes :

- conclusion ou enchaînements : donc, puisque, mais encore, c'est ainsi que, alors, certes, par ailleurs,
- articulations contradictoires : or, cependant,
- compléments : c'est-à-dire,


### 1.3. Formules, expressions, concepts soulignés par l'auteur (en italique, gras, etc.)
Gilbert Simondon utilise peu de moyens typographiques pour mettre en valeur certains arguments ou certaines idées. L'italique permet de signaler certains concepts, mais il s'agit principalement d'emphases pour appuyer une phrase, un passage, un mot.

Quelques expressions soulignées par l'auteur, il n'y en a que deux dans la première partie du chapitre 1 :

- "la forme abstraite" (page 24)
- "c'est dans les incompatibilités naissant de la saturation progressive du système de sous-ensembles que réside le jeu de limites dans le franchissement constitue un progrès" (page 32)

### 1.4. Ponctuation significative
Certaines phrases sont longues, articulées par des `;`. Il n'y a pas d'autres ponctuation significative à noter.

### 1.5. Structure première
La structure générale est classique, en trois niveaux : partie puis chapitre puis sous-partie. Chaque sous-partie est relativement courte, et toujours cohérente : développement d'un argument qui regroupe des idées et des concepts, idées et concepts qui seront ensuite réutilisés dans les parties suivantes.

Dans chaque sous-partie (troisième niveau), Gilbert Simondon commence par poser une question, il y répond mais il répond également à des questions plus générales qui découlent elles-mêmes de


## 2. Étude conceptuelle

### 2.1. Repérage des termes essentiels
Voici les principaux termes utilisés par Gilbert Simondon :

- nature, culture
- objet technique
- aliénation
- sacralisation
- outil, ustensile
- automatisme
- indétermination
- ouverture, machine ouverte
- chef d'orchestre
- forme abstraite
- concrétisation
- objet concret, objet abstrait
- artisanat, industrie
- réorganisation structurale
- synergie
- progrès technique
- genèse
- lignée technique
- hypertélie
- milieu
- autonomie
- adaptation
- plurifonctionnalité
- invention
- milieu associé
- être technique
- fond, forme
- causalités récurrentes
- laboratoire
- élément technique, individu technique, ensemble technique
- loi de relaxation
- négativité
- technicité
- forme et matière
- progrès technique, puissance
- imagination technique
- technicien, artisan
- machine-outil
- culture technique

### 2.2. Définition des termes et concepts (dans le cadre du texte)
#### Objet technique
Outil, ustensile, machine, machine-outil, automate, l'objet technique peut être tout cela à la fois. Toutefois il se caractérisera par son niveau de concrétisation et d'individualisation. Une machine est un objet technique si ...

#### Outil, ustensile
Un outil présente plusieurs caractéristiques : il a une fonction, et cette fonction est activée par une intervention qui n'est pas celle de l'outil. L'homme peut activer cette fonction, mais cela peut également être le cas d'une machine. L'outil, par rapport à l'objet technique, présente plusieurs différences : il ne connaît pas de forcément de concrétisation, et il n'est pas individualisé. L'outil peut représenter un progrès technique non pas dans sa ou ses fonctions, mais dans la technicité de sa composition : assemblage des pièces, constitution des pièces (alliage, découpe du bois), etc.

#### Automatisme
Gilbert Simondon aborde l'automatisme pour critiquer la représentation qui en est faite : il ne faut pas considérer l'automatisme comme étant le moyen pour les machines de se suffire à elles-mêmes, et il ne faut pas non plus considérer l'automatisme comme le prolongement de l'homme en ce qui lui permet de ne plus affronter la vie.

#### Machine ouverte
Terme lié à l'automatisme : une machine parfaitement autonome ne présente aucun intérêt, la machine ouverte est celle qui peut être orchestrée par l'homme, en tant que l'homme est au même niveau que les machines.

#### Concrétisation, objet concret
Un processus de concrétisation consiste en une imbrication de fonctions : chaque élément remplit plusieurs fonctions, et s'interconnecte donc avec d'autres éléments. Au lieu d'être une série de fonctions uniques, l'objet concret regroupe des éléments qui interagissent entre eux. Plutôt que d'ajouter des éléments – des sortes de *patchs* – pour être plus efficient, l'objet technique intègre totalement l'élément ou la fonction, il trouve une cohérence globale. La concrétisation est un objectif à atteindre, une tendance, dans le cas des objets techniques, alors que les objets naturels sont concrets par essence.  
La concrétisation s'oppose à l'abstraction, dans le cas de l'abstraction l'objet technique est abstrait de son environnement de fonctionnement.

#### Adaptation
C'est la possibilité, pour un objet technique, de ne pas répondre uniquement à un usage, un objectif précis. Il y a adaptation si l'objet technique est capable d'un certain *déséquilibre*, s'il peut répondre à un autre usage que celui pour lequel il a été pensé, si l'homme a permis à un milieu associé d'exister.

#### Hypertélie
L'hypertélie, pour Gilbert Simondon, est une caractéristique qui se définit par une spécialisation qui rend l'objet technique moins *adaptable*. En devenant spécialisé, l'objet technique n'est plus capable de répondre à d'autres usages. L'hypertélie peut être de plusieurs sortes : spécialisation de l'objet technique, en conservant l'unité de celui-ci ; "fractionnement" de l'objet technique, en vue d'une spécialisation ; dépendance de l'objet technique à un milieu très précis pour pouvoir être efficient. L'hypertélie doit être évitée, puisque le progrès technique n'est possible que lorsqu'il y a adaptation-concrétisation de l'objet technique, et donc lorsque l'objet technique est en capacité de s'adapter. L'hypertélie est également la manifestation d'une mauvaise adéquation entre l'objet technique et le milieu.

#### Milieu associé
Ce milieu, naturel et technique, est en quelque sorte créé par l'objet technique lui-même, c'est le milieu qui résulte du fonctionnement de l'objet technique en tant qu'il y a des causalités récurrentes.  
"C'est ce milieu associé qui est la condition d'existence de l'objet technique inventé."  
La différence entre un être vivant et un être technique réside dans le fait que l'être vivant porte son milieu associé, alors que l'objet technique a besoin d'une intervention extérieure pour que ce milieu associé apparaisse.  
Dernier point important concernant le milieu associé : il est homéostatique, c'est-à-dire que les informations extérieures n'auront pas forcément d'incidences sur le fonctionnement, les structures ont une certaine indépendance. {Gros doute sur cette dernière partie de la définition.}

#### Causalités récurrentes
Les causalités récurrentes correspondent aux interactions entre fonctions ou éléments : les actions entraînent des actions, les éléments ne sont pas isolés dans l'objet technique.  
La récurrence de causalité est la condition du milieu associé (voir ci-dessus), et elle est au niveau de l'individu technique (et non au niveau *supérieur*).

#### Élément technique, individu technique, ensemble technique
Gilbert Simondon distingue trois niveaux :

- les éléments techniques, une fois assemblés, constituent l'individu technique. Les éléments techniques sont des outils ;
- l'individu technique est le niveau du milieu associé. L'individu technique porte les outils, c'était le cas de l'homme lorsque les objets techniques n'avaient pas atteint un niveau suffisant de concrétisation ;
- l'ensemble technique regroupe des individus techniques, il s'agit de l'arrivée de la "théorie de l'information", et de l'automatisme.

La question est : à quel niveau est l'objet technique ? Cela dépend des périodes historiques.

Toute la thèse de Gilbert Simondon repose sur cela : l'homme doit se repositionner par rapport aux objets techniques.

#### Technicité
"La technicité est le degré de concrétisation de l'objet." C'est donc le degré d'imbrication des fonctions d'un objet : plus les fonctions se *répondent* et intéragissent entre elles, et plus un objet technique a un niveau important de technicité. Il ne s'agit pas uniquement de la "qualité d'usage", mais un haut degré de perfection des éléments techniques qui composent l'objet technique.

#### Machine-outil
la machine-outil ne fait que porter les outils, et l'homme fait fonctionner la machine-outil. L'objet technique va plus loin que la machine-outil, en tant qu'il y a concrétisation et individualisation.


### 2.3. Établir les rapports entre les différentes parties
La fiche de lecture ne concerne que la première partie : "Genèse et évolution des objets techniques" :

#### Chapitre 1 – Genèse de l'objet technique : le processus de concrétisation
Résumé : dans ce chapitre Gilbert Simondon présente principalement le concept de concrétisation, sous la forme d'une ontogenèse.

Détails par sous-parties :

1. Forme abstraite de l'objet technique : en tant qu'il est dissocié de son environnement de fonctionnement. Évolution de l'objet technique vers une convergence de ses fonctions : de l'abstrait vers le concret. Articulation : qu'est-ce qu'un objet technique concret ?
2. L'objet technique abstrait est un ensemble de fonctions autonomes, alors que l'objet concret est un ensemble de fonctions imbriquées et non isolées. Il faut aussi considérer que certaines éléments auront plusieurs fonctions qui ne contredisent ni ne s'annulent, il s'agit là d'une synergie. La concrétisation est un horizon à atteindre. Articulation : comment fonctionne cette évolution ?
3. L'évolution, ou plutôt le progrès technique, n'est pas continue, mais brusque : les évolutions mineures – améliorations de l'objet technique – permettent des évolutions majeures – nouvel objet technique. Il ne faut pas être fonctionnaliste, la perfection d'un objet technique ne correspond pas à son degré de résolution d'un problème mais à son propre fonctionnement. Articulation : quelle forme prend cette évolution ?
4. Il ne faut pas parler de genèse, mais de filiation : l'évolution mineure de certains objets techniques donnent lieu à de nouveaux objets techniques, une évolution majeure. Articulation : comment est permise cette évolution ? Par l'humain, et comment ?

#### Chapitre 2 – Évolution de la réalité technique ; élément, individu, ensemble
Résumé :

Détails :

1. L'hypertélie de l'objet technique est un phénomène de spécialisation qui le rend moins adaptable, et moins autonome. La plurifonctionnalité des objets techniques n'est possible que sans une spécialisation. Cela est rendu possible dans un certain milieu, le troisième milieu : ni l'objet technique seul, ni son contexte. Qu'est-ce que ce troisième milieu ?
2. L'individualisation de l'objet technique est permise par le "milieu associé", milieu créé par l'objet technique et qui permet à l'objet technique d'exister. Il ne doit pas y avoir de rupture entre fond – schéma global, stabilité – et formes – série d'événements. Mais ce milieu associé est permis par l'homme, qui invente cette condition : prévoir ce qui se passera dans une situation d'application. La causalité récurrente est une condition de l'individualisation, mais à quel niveau ?
3. Distinction importante entre individus techniques et ensemble d'individus : l'individualisation ne peut intervenir au niveau de l'ensemble, mais l'ensemble *hérite* de cette individualisation, tout comme pour la causalité récurrente. Le niveau inférieur d'individualité existe, il s'agit de l'élément technique, sans milieu associé mais en capacité d'intégrer l'individu technique. Comment se déroule cette *passation* ?
4. Passage de causalité entre différents niveaux : entre élément technique et individu technique. Comparaison entre vivant et technique : élément technique détachable, alors que l'organe pas vraiment ; distinction engendré et produit ; évolution continue et en "dents de scie". Il s'agit du temps de relaxation : évolution non linéaire due à cette distinction et à ce passage de causalité.
5. Production "indirecte" : améliorations successives qui donnent lieu à des nouveaux objets techniques, alors que les êtres vivants se reproduisent à l'identique. C'est par le jeu des imperfections que les objets techniques évoluent. Le point commun entre la machine et l'homme réside dans la notion d'individu, qui peut être considérée comme l'auto-régulation : il y a l'idée d'un fonctionnement autonome, et par exemple la machine-outil ne fait qu'utiliser des outils, alors que l'objet technique est plus indépendant, Gilbert Simondon fait également la distinction entre technicien et artisan pour appuyer cette distinction. La question est : quel est le niveau d'intervention de l'homme ? Pas au niveau de l'individu technique, puisque l'homme a dépassé cela, donc : avant, au niveau de l'élément technique, et après, au niveau de l'ensemble technique. Introduction de la culture technique.


### 2.4. Structure dynamique (les parties, le cheminement du raisonnement et de l'argumentation)


## 3. Thème et thèse
### 3.1. Thème
Au premier abord : la technique, en tant qu'elle est mal perçue. Mais en approfondissant la lecture : il s'agit de la culture dans laquelle la technique doit être considérée comme un mode de rapport au monde, parmi d'autres mais essentielle.

### 3.2. Thèse
La technique est mal considérée, "notre civilisation est mal technicienne" et non trop technicienne : il y a des objets sacrés ou des objets esthétiques, mais les objets techniques sont envisagés comme étant en dehors de la culture. Gilbert Simondon veut que la technique soit appréhendée, connue, afin de la considérer
Comment envisager un horizon culturel neuf ? Pour cela il faut considérer la technique comme jonction entre le vital et les institutions culturelles.

## 4. Problématique
### 4.1. Questionnement
### 4.2. Problème
### 4.3. Enjeu(x)

## 5. Partie réflexive
### 5.1. Situation du texte dans l'histoire des idées
Opposition par rapport à Martin Heidegger, qui ne pense pas la technique à partir de la catégorie de l'objet.

Pas de positionnement par rapport à un statut moral de la technique, contrairement à Jacques Ellul par exemple.

Suite : Bernard Stiegler ou Pierre-Damien Huyghe.

### 5.2. Intérêt philosophique du problème (et de l'éventuelle solution de l'auteur)
### 5.3. Étude de la portée du fragment
### 5.4. Éventuellement, commentaires divers
