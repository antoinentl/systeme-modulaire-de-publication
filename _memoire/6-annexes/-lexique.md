Ce lexique constitue un outil pour la lecture de ce mémoire.
Pour définir les termes présentés ci-dessous plusieurs ressources ont été utilisées, dont l'encyclopédie Wikipédia, le portail lexical du Centre national de ressources textuelles et lexicales, ainsi que les ouvrages et documents présentés dans la bibliographie en annexe.
Lorsque des paraphrases ou des citations sont utilisées dans les définitions, une référence est précisée.

#### Asciidoc
AsciiDoc est un langage de balisage léger et le nom du programme qui permet de convertir des fichiers en AsciiDoc dans d'autres formats.
Certains livres d'O'Reilly Media ont été fabriqués avec ce langage.
Depuis 2013 la suite de programmes Asciidoctor tend à remplacer AsciiDoc en tant que programme.

#### BibTeX
BibTeX est un format de fichier permettant de gérer une base bibliographique, il a été conçu par Leslie Lamport et Oren Patashnik en 1985, originellement pour LaTeX même si aujourd'hui il est utilisé dans d'autres systèmes.

#### Chaîne de publication, chaîne d'édition, chaîne éditoriale
Une chaîne de publication, aussi appelée chaîne d'édition ou chaîne éditoriale, est un ensemble de méthodes et d'outils ordonnés qui remplissent des objectifs déterminés pour un projet éditorial.

<div class="break"></div>
#### Commit
Au sein du système de gestion de versions Git, un _commit_ est un enregistrement accompagné d'un message, cela correspond à un état d'un projet géré avec Git.

#### CSS
_Cascading Style Sheets_, feuilles de style en cascade en français, est un langage informatique décrivant la mise en forme de documents structurés avec HTML.
CSS est un standard défini par le World Wide Web Consortium (W3C).

#### EPUB
Le format EPUB, pour _electronic publication_, est le format du livre numérique.
Ce standard, maintenu par l'International Digital Publishing Forum (IDPF) désormais intégré au W3C, permet de créer des publications numériques portables et reconfigurables.
Il est possible de comparer le format EPUB à un site web encapsulé.

#### Générateur de site statique
Un générateur de site statique, _static site generator_ ou SSG en anglais, est un programme permettant de transformer des fichiers écrits dans un format de langage de balisage léger en fichiers HTML organisés constituant un site web.
Contrairement au système de gestion de contenu classique comme le CMS Wordpress, les générateurs de site statique n'utilisent pas de bases de données.
Plus d'informations via le site de la communauté française Jamstatic : https://jamstatic.fr/.

#### Homothétique
L'homothétie caractérise deux situations similaires.
Habituellement utilisé pour le livre numérique, ce qualificatif détermine la similitude entre un livre imprimé et son homologue numérique.
Le livre numérique homothétique s'oppose au livre numérique enrichi qui peut comporter des éléments multimédias et des options de navigation avancées – et non simplement le feuilletage.

<div class="break"></div>
#### HTML
Hypertext Markup Language, langage de balisage hypertextuel en français, est un langage permettant de structurer un document qui est ensuite interprété par un navigateur web.
HTML est associé à une feuille de style CSS pour pouvoir personnaliser la composition.

#### LaTeX
LaTeX est l'association d'un langage de balisage, TeX, et d'un processeur qui génère différents formats à partir de cette source {% cite rouquette_xelatex_2012 %}.
Créé par Leslie Lamport en 1983 à partir de TeX de Donald Knuth — lui-même créé en 1977 —, LaTeX est pensé pour l'impression de documents — article, thèse, livre – et donc la production de fichiers PDF.
LaTeX a précédé les interfaces à base de WYSIWYG.

#### Langage de balisage léger
Les langages de balisage léger sont l'application du mode WYSIWYM, le plus célèbre et répandu d'entre eux est Markdown.
"Un langage de balisage n’est rien d’autre qu’un système d’annotation d’un document qui s’effectue d’une telle manière qu’il est possible de distinguer ces balises, ces annotations, du texte que l’on annote." {% cite dehut_en_2018 %}

#### Libre
Sont qualifiés de "libres" des logiciels ou des programmes qui répondent aux quatre critères suivants : "l'utilisation, l'étude, la modification et la duplication par autrui en vue de sa diffusion sont permises, techniquement et légalement" (source : Wikipédia).
Il faut distinguer "libre" d'"open source".

#### Livre web, web book
Un livre web, ou _web book_ en anglais, est un un livre sous la forme d’un site web.
Il s'agit d'un ensemble de pages structurées, organisées et rendues accessibles par un menu ou une table des matières ; les contenus s’adaptent formellement à la taille de l’écran, responsifs ou adaptatifs ; des liens hypertextes permettent de naviguer dans et en-dehors du livre ; certaines formes de "livre web" peuvent être consultées hors ligne {% cite fauchie_livre_2016 %}.

#### Logiciel de publication assistée par ordinateur
Aussi appelé logiciel de PAO ou logiciel de composition, un logiciel de publication assistée par ordinateur permet de mettre en forme des documents dans l'objectif de les imprimer.
InDesign est l'un des logiciels les plus connus et les plus utilisés.

#### Maintenabilité
La maintenabilité, en informatique, concerne la faculté à maintenir les composants d'un système pour permettre à ce dernier de fonctionner.

#### Markdown
"Inventé par John Gruber au début des années 2000, Markdown est un langage sémantique qui permet d'écrire du HTML — Hyper Text Markup Language — avec un système de balisage bien plus léger. D'abord plébiscité par les développeurs pour rédiger leur documentation, cette syntaxe est désormais de plus en plus employée, notamment dans des applications numériques qui cherchent à se passer d'interfaces WYSIWYG — What You See Is What You Get." {% cite fauchie_markdown_nodate %}

#### Prince XML
Prince XML ou Prince est un logiciel propriétaire qui convertit des fichiers au format XML ou HTML en fichiers au format PDF en appliquant une feuille de style CSS.

#### Open source
_Open source_, ou code ouvert en français, désigne les possibilités de libre redistribution, d'accès au code source et de création de travaux dérivés pour un programme ou logiciel informatique.
Si la démarche "libre" est un projet humain et politique, celle de l'open source vise avant tout l'évolution technique.

<div class="break"></div>
#### Traitement de texte
Un traitement de texte, ou _word processor_ en anglais, est un logiciel dédié à l'écriture.
Il permet, en théorie, de structurer et de mettre en forme un document.
Dans les faits, le traitement de texte à tendance à favoriser une confusion entre structure et mise en forme {% cite reichenstein_multichannel_2016 %}.

#### Vivliostyle
Vivliostyle est un programme permettant de convertir des fichiers HTML en appliquant une feuille de style CSS.
C'est un concurrent libre de PrinceXML.

#### WYSIWYG
_What You See Is What You Get_, ce que vous voyez est ce que vous obtenez en français.
Ce mode d'édition a été mis en place avec les premières interfaces graphiques dans le domaine informatique, facilitant d'abord la vie aux utilisateurs, puis étant critiqué pour la confusion qu'il crée entre structure et mise en forme.

#### WYSIWYM
_What You See Is What You Mean_, ce que vous voyez est ce que vous signifiez en français.
Il s'agit de l'approche alternative au WYSIWYG redonnant du sens à l'acte d'inscription en indiquant clairement la structure du document et en se focalisant moins sur les aspects graphiques.

#### XML
_Extensible Markup Language_, ou langage de balisage extensible en français, est un langage informatique permettant de structurer du texte avec l'association d'une définition de type de document.

#### XML TEI
TEI, pour _Text Encoding Initiative_ ou Initiative pour l’encodage du texte en français, définit un document type définition pour XML.

#### YAML
YAML, pour _YAML Ain't Markup Language_, est un format de représentation de données.
