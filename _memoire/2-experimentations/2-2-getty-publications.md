---
title: "2.2. Getty Publications"
category: 2. Expérimentations
order: 3
partiep_link: /2-experimentations/2-1-o-reilly/
parties_link: /2-experimentations/2-3-distill/
repo: _memoire/2-experimentations/-2-2-getty-publications.md
---
{% include_relative -2-2-getty-publications.md %}
