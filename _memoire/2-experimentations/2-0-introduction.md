---
title: "Introduction"
category: 2. Expérimentations
order: 1
redirect_from:
  - /2-experimentations/
partiep_link: /1-mutations-du-livre/1-3-progres-technique/
parties_link: /2-experimentations/2-1-o-reilly/
repo: _memoire/2-experimentations/-2-0-introduction.md
---
{% include_relative -2-0-introduction.md %}
