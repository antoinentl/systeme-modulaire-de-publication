---
title: Page non trouvée
permalink: /404.html
sitemap: false
---
Vous vous êtes égaré·e.

Avant de tenter de retrouver votre chemin, voici une citation de Gilbert Simondon :

>[...] l'objet technique progresse par redistribution intérieure des fonctions en unités compatibles, remplaçant le hasard ou l'antagonisme de la répartition primitive ; la spécialisation ne se fait pas *fonction par fonction*, mais *synergie par synergie*.
>{% cite simondon_du_2012 %}
